--[[

Awesome WM configuration template
based on github.com/lcpz

--]]

-- {{{ Required libraries
local awesome, client, mouse, screen, tag = awesome, client, mouse, screen, tag
local ipairs, string, os, table, tostring, tonumber, type = ipairs, string, os, table, tostring, tonumber, type

local gears         = require("gears")
local awful         = require("awful")
require("awful.autofocus")
local wibox         = require("wibox")
local beautiful     = require("beautiful")
local naughty       = require("naughty")
local lain          = require("lain")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local my_table      = awful.util.table or gears.table -- 4.{0,1} compatibility
local dpi           = require("beautiful.xresources").apply_dpi
-- }}}

local config        = require("config")

-- TODO: vpn

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({ preset = naughty.config.presets.critical,
  title = "Oops, there were errors during startup!",
  text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function (err)
    if in_error then return end
    in_error = true

    naughty.notify({ preset = naughty.config.presets.critical,
    title = "Oops, an error happened!",
    text = tostring(err) })
    in_error = false
  end)
end
-- }}}

-- {{{ Autostart windowless processes

-- This function will run once every time Awesome is started
local function run_once(cmd_arr)
  for _, cmd in ipairs(cmd_arr) do
    awful.spawn.with_shell(string.format("pgrep -u $USER '%s' > /dev/null || %s", cmd, cmd))
  end
end

run_once({ "firefox", "Discord", "slack", "lunettes-fumees" })

-- }}}

local modkey       = "Mod4"
local altkey       = "Mod1"
local terminal     = "alacritty"
local browser      = "firefox"
local scrlocker    = "/home/adminxvii/.local/bin/lock"
local wifi_menu    = "/home/adminxvii/.local/bin/wifi-menu"

awful.util.terminal = terminal
awful.util.tagnames = { "browser", "terms", "social", "steam" }
awful.layout.layouts = {
  awful.layout.suit.max,
  --awful.layout.suit.tile.bottom,
  awful.layout.suit.tile,
  --awful.layout.suit.floating,
  --awful.layout.suit.tile.left,
  --awful.layout.suit.tile.top,
  --awful.layout.suit.fair,
  --awful.layout.suit.fair.horizontal,
  --awful.layout.suit.spiral,
  --awful.layout.suit.spiral.dwindle,
  --awful.layout.suit.max.fullscreen,
  --awful.layout.suit.magnifier,
  --awful.layout.suit.corner.nw,
  --awful.layout.suit.corner.ne,
  --awful.layout.suit.corner.sw,
  --awful.layout.suit.corner.se,
  --lain.layout.cascade,
  --lain.layout.cascade.tile,
  --lain.layout.centerwork,
  --lain.layout.centerwork.horizontal,
  --lain.layout.termfair,
  --lain.layout.termfair.center,
}

lain.layout.termfair.nmaster           = 3
lain.layout.termfair.ncol              = 1
lain.layout.termfair.center.nmaster    = 3
lain.layout.termfair.center.ncol       = 1
lain.layout.cascade.tile.offset_x      = 2
lain.layout.cascade.tile.offset_y      = 32
lain.layout.cascade.tile.extra_padding = 5
lain.layout.cascade.tile.nmaster       = 5
lain.layout.cascade.tile.ncol          = 2

naughty.config.defaults.border_width            = beautiful.notification_border_width
naughty.config.defaults.margin                  = beautiful.notification_margin
beautiful.init(string.format("%s/.config/awesome/theme.lua", os.getenv("HOME")))
-- }}}

-- {{{ Screen
-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", function(s)
  -- Wallpaper
  if beautiful.wallpaper then
    local wallpaper = beautiful.wallpaper
    -- If wallpaper is a function, call it with the screen
    if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)
  end
end)
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)
-- }}}

-- This snippet is a code I wrote to configure the position of monitors with AwesomeWM
-- Usage :
-- Press your Display button (or whatever you binded it to)
-- Popup will show the name of a connected monitor
-- Press an arrow key or Display button to set this monitor in this direction relatively to your main monitor, (display for same position)
-- It will continue until you have configured all connected monitors

-- the name of your main screen as shown in xrandr
main_screen = "eDP-1"

-- Helper function to conf a screen
function conf_screen(screen, mode, pos)
  if screen == nil then cmd = "xrandr --auto"
  else
    cmd = "xrandr --output " .. screen .. " --mode " .. mode .. " --" .. pos .. " " .. main_screen
  end
  naughty.notify { text = cmd }
  awful.spawn(cmd)
end

function manage_screens ()
  -- Command to get list of other screens and best mode
  -- hack to handle when monitors mode are not sorted, like my TV does.
  local get_screens = "xrandr | awk '/ connected/{screen=$1;a=1;next}/connected/{a=0}a{print screen \" \" $1}' | sort -k2n | tac | grep -v i | sort -u -k1,1 | grep -v " .. main_screen
  awful.spawn.easy_async_with_shell(get_screens,
  function(stdout, stderr, reason, exit_code)

    -- Process the results
    screens = {}
    for i in stdout:gmatch("[^\r\n]+") do
      a, b = i:match("(%S+) (%S+)")
      table.insert(screens, {a, b})
    end

    -- If no screens, reset conf
    if #screens == 0 then conf_screen(); return end

    -- For each screens, ask position
    local idx = 1
    local notif = naughty.notify { text = "Position for " .. screens[idx][1] }
    local grabber
    grabber = awful.keygrabber.run(function(mod, key, event)
      if event == "release" then return end
      naughty.notify { text = "Key " .. key .. " - " .. screens[idx][1] .. " : " .. screens[idx][2] }

      if key == 'Up'      then conf_screen(screens[idx][1], screens[idx][2], "above")
      elseif key == 'Down'    then conf_screen(screens[idx][1], screens[idx][2], "below")
      elseif key == 'Right'   then conf_screen(screens[idx][1], screens[idx][2], "right-of")
      elseif key == 'Left'    then conf_screen(screens[idx][1], screens[idx][2], "left-of")
      elseif key == 'Return'  then return
      else conf_screen(screens[idx][1], screens[idx][2], "same-as")
      end

      if idx < #screens then
        idx = idx + 1
        naughty.destroy(notif)
        notif = naughty.notify { text = "Position for " .. screens[idx][1] }
      else
        naughty.destroy(notif)
        awful.keygrabber.stop(grabber)
      end
    end)
  end)
end

-- {{{ Key bindings
globalkeys = my_table.join(
-- Take a screenshot
-- https://github.com/lcpz/dots/blob/master/bin/screenshot
awful.key({ modkey }, "p", function() awful.spawn({ "scrot", "/home/adminxvii/Pictures/Screenshots/%j-%T.png", "-e", "xclip -selection clipboard -target image/png -i $f"}) end,
{description = "take a screenshot", group = "hotkeys"}),

-- X screen locker
awful.key({ modkey }, "l", function () awful.spawn(scrlocker) end,
{description = "lock screen", group = "hotkeys"}),

-- Hotkeys
awful.key({ modkey }, "s",      hotkeys_popup.show_help,
{description = "show help", group="awesome"}),

-- Manage screens
awful.key({ modkey }, "d", manage_screens,
{description = "Display management auto", group = "function keys"}),

-- By direction client focus
awful.key({ modkey, "Shift" }, "t",
function()
  awful.client.focus.global_bydirection("down")
  if client.focus then client.focus:raise() end
end,
{description = "focus client", group = "client"}),
awful.key({ modkey, "Shift" }, "s",
function()
  awful.client.focus.global_bydirection("up")
  if client.focus then client.focus:raise() end
end,
{description = "focus client", group = "client"}),
awful.key({ modkey, "Shift" }, "c",
function()
  awful.client.focus.global_bydirection("left")
  if client.focus then client.focus:raise() end
end,
{description = "focus client", group = "client"}),
awful.key({ modkey, "Shift" }, "r",
function()
  awful.client.focus.global_bydirection("right")
  if client.focus then client.focus:raise() end
end,
{description = "focus client", group = "client"}),

-- Layout manipulation
awful.key({ modkey }, "Left", function () awful.client.swap.byidx(  1)    end,
{description = "swap client", group = "client"}),
awful.key({ modkey }, "Right", function () awful.client.swap.byidx( -1)    end,
{description = "swap client", group = "client"}),
awful.key({ modkey, "Shift" }, "Tab", function () awful.screen.focus_relative( 1) end,
{description = "focus screen", group = "screen"}),
awful.key({ modkey,         }, "u", awful.client.urgent.jumpto,
{description = "jump to urgent client", group = "client"}),
awful.key({ modkey,         }, "Tab", function () awful.client.focus.byidx(1) end,
{description = "go back", group = "client"}),

-- Dynamic tagging
awful.key({ modkey, "Shift" }, "n", function ()
  local screen = awful.screen.focused()
  local count = #screen.tags
  if count < 9 then
    local name = "extra-" .. (count - 3)
    awful.tag.add(name, { screen = screen, layout = awful.layout.suit.tile }):view_only()
  end
end,
{description = "add new tag", group = "tag"}),
-- awful.key({ modkey, "Shift" }, "r", function () lain.util.rename_tag() end,
-- {description = "rename tag", group = "tag"}),
awful.key({ modkey, "Shift" }, "d", function ()
  lain.util.delete_tag()
  local screen = awful.screen.focused()
  for idx = 5, #screen.tags do
      screen.tags[idx].name = "extra-" .. (idx - 4)
  end
end,
{description = "delete tag", group = "tag"}),

-- Standard program
awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
{description = "open a terminal", group = "launcher"}),
awful.key({ modkey, "Control" }, "r", awesome.restart,
{description = "reload awesome", group = "awesome"}),
awful.key({ modkey, "Shift"   }, "q", awesome.quit,
{description = "quit awesome", group = "awesome"}),

awful.key({ modkey }, "-",     function () awful.tag.incmwfact( 0.05)          end,
{description = "increase master width factor", group = "layout"}),
awful.key({ modkey }, "+",     function () awful.tag.incmwfact(-0.05)          end,
{description = "decrease master width factor", group = "layout"}),
awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
{description = "select next", group = "layout"}),
awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
{description = "select previous", group = "layout"}),

-- Widgets popups
awful.key({ modkey, }, "w", function () awful.spawn(wifi_menu) end,
{description = "show menu control", group = "widgets"}),

-- Brightness
awful.key({ }, "XF86MonBrightnessUp", function () awful.spawn("light -A 5") end,
{description = "+10%", group = "hotkeys"}),
awful.key({ }, "XF86MonBrightnessDown", function () awful.spawn("light -U 5") end,
{description = "-10%", group = "hotkeys"}),

-- ALSA volume control
awful.key({ }, "XF86AudioRaiseVolume",
function ()
  awful.spawn(string.format("amixer -q set %s 1%%+", beautiful.volume.channel))
  beautiful.volume.update()
end,
{description = "volume up", group = "hotkeys"}),
awful.key({ }, "XF86AudioLowerVolume",
function ()
  awful.spawn(string.format("amixer -q set %s 1%%-", beautiful.volume.channel))
  beautiful.volume.update()
end,
{description = "volume down", group = "hotkeys"}),
awful.key({ }, "XF86AudioMute",
function ()
  awful.spawn(string.format("amixer -q -D pulse set Master 1+ toggle", beautiful.volume.togglechannel or beautiful.volume.channel))
  beautiful.volume.update()
end,
{description = "toggle mute", group = "hotkeys"}),

-- User programs
awful.key({ modkey }, "b", function () awful.spawn(browser) end,
{description = "run browser", group = "launcher"}),
awful.key({ modkey }, "e", function () awful.spawn("steam") end,
{description = "run steam", group = "launcher"}),

-- Prompt
awful.key({ modkey }, "r", function () awful.spawn({ "rofi", "-show"}) end,
{description = "run prompt", group = "launcher"})
)

clientkeys = my_table.join(
awful.key({ modkey,           }, "f",
function (c)
  c.maximized_horizontal = false  -- fix libreoffice
  c.maximized_vertical = false
  c.maximized = false
  c.floating = false
  c.fullscreen = not c.fullscreen
  c:raise()
end,
{description = "toggle fullscreen", group = "client"}),
awful.key({ modkey }, "q",      function (c) c:kill()                         end,
{description = "close", group = "client"}),
awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle,
{description = "toggle floating", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
  -- Hack to only show tags 1 and 9 in the shortcut window (mod+s)
  local descr_view, descr_toggle, descr_move, descr_toggle_focus
  if i == 1 or i == 9 then
    descr_view = {description = "view tag #", group = "tag"}
    descr_toggle = {description = "toggle tag #", group = "tag"}
    descr_move = {description = "move focused client to tag #", group = "tag"}
    descr_toggle_focus = {description = "toggle focused client on tag #", group = "tag"}
  end
  globalkeys = my_table.join(globalkeys,
  -- View tag only.
  awful.key({ modkey }, "#" .. i + 9,
  function ()
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      tag:view_only()
    end
  end,
  descr_view),
  -- Toggle tag display.
  awful.key({ modkey, "Control" }, "#" .. i + 9,
  function ()
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      awful.tag.viewtoggle(tag)
    end
  end,
  descr_toggle),
  -- Move client to tag.
  awful.key({ modkey, "Shift" }, "#" .. i + 9,
  function ()
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:move_to_tag(tag)
      end
    end
  end,
  descr_move),
  -- Toggle tag on focused client.
  awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
  function ()
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:toggle_tag(tag)
      end
    end
  end,
  descr_toggle_focus)
  )
end

clientbuttons = gears.table.join(
awful.button({ }, 1, function (c)
  c:emit_signal("request::activate", "mouse_click", {raise = true})
end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
  -- All clients will match this rule.
  {
    rule = { },
    properties = {
      focus = awful.client.focus.filter,
      raise = true,
      keys = clientkeys,
      buttons = clientbuttons,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap+awful.placement.no_offscreen,
      size_hints_honor = false
    }
  },

  -- Titlebars
  { rule = { class = "Firefox" },
  properties = { screen = 1, tag = awful.util.tagnames[1] } },

  { rule = { class = "Steam" },
  properties = { screen = 1, tag = awful.util.tagnames[4] } },

  { rule = { class = "Slack" },
  properties = { screen = 1, tag = awful.util.tagnames[3] } },
  
  { rule = { class = "discord" },
  properties = { screen = 1, tag = awful.util.tagnames[3] } },
}
-- }}}

  -- {{{ Signals
  -- Signal function to execute when a new client appears.
  client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
      -- Prevent clients from being unreachable after screen count changes.
      awful.placement.no_offscreen(c)
    end
    c.shape = function(cr,w,h)
      gears.shape.rounded_rect(cr,w,h,10)
    end
  end)
  -- }}}

  -- Enable sloppy focus, so that focus follows mouse.
  client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = true})
  end)
  -- }}}
