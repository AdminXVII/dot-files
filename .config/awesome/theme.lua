-- With github.com/lcpz as reference

local gears  = require("gears")
local lain   = require("lain")
local awful  = require("awful")
local wibox  = require("wibox")
local dpi    = require("beautiful.xresources").apply_dpi
local config = require("config")

local string = string
os.setlocale("fr_CA.UTF-8")
local os = { getenv = os.getenv }
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}
theme.default_dir                               = require("awful.util").get_themes_dir() .. "default"
theme.icon_dir                                  = os.getenv("HOME") .. "/.config/awesome/icons"
theme.wallpaper                                 = os.getenv("HOME") .. "/.config/awesome/wall-fleur.jpg"
theme.font                                      = "DejaVu Sans Mono 8"
theme.taglist_font                              = "DejaVu Sans Mono 7"
theme.fg_normal                                 = "#9893a5"
theme.fg_focus                                  = "#575279"
theme.bg_focus                                  = "#30303000"
theme.bg_normal                                 = "#faf4ed"
theme.fg_urgent                                 = "#b4637a"
theme.bg_urgent                                 = "#f2e9e1"
theme.bg_systray                                = theme.bg_normal
theme.border_width                              = 0
theme.border_normal                             = "#d7827e"
theme.border_focus                              = theme.fg_focus
theme.taglist_bg_focus                          = "##79759340"
theme.useless_gap                               = dpi(4)
theme.notification_font                         = "Roboto Condensed 10"
theme.notification_bg                           = theme.bg_normal
theme.notification_fg                           = theme.fg_normal
theme.notification_border_width                 = dpi(2)
theme.notification_border_color                 = "#ea9d34" -- theme.notification_fg
theme.notification_shape                        = function(cr, width, height) gears.shape.rounded_rect(cr, width, height, 10) end
theme.notification_margin                       = dpi(30)
theme.notification_icon_size                    = dpi(50)

theme.musicplr = string.format("%s -e ncmpcpp", awful.util.terminal)

local markup = lain.util.markup

-- Calendar
local textcal = wibox.widget.textclock(markup.font(theme.font, "%A %d %b, %H:%M"))
theme.cal = lain.widget.cal({
  attach_to = { textcal },
  notification_preset = {
    font = "DejaVu Sans Mono 9",
    position = "bottom_right",
  }
})

-- Mail IMAP check
--[[ commented because it needs to be set before use
theme.mail = lain.widget.imap({
timeout  = 180,
server   = "server",
mail     = "mail",
password = "keyring get mail",
settings = function()
mail_notification_preset.fg = "#FFFFFF"
mail  = ""
count = ""

if mailcount > 0 then
mail = "Mail "
count = mailcount .. " "
end

widget:set_markup(markup.font(theme.font, markup(blue, mail) .. markup("#FFFFFF", count)))
end
})
--]]

-- Net
local net = lain.widget.net {
    notify = "off",
    settings = function()
    	function unit(x)
        	local num = tonumber(x)
        	if num > 1000000 then
            	return string.format("%.1f", num / 1000000) .. " G"
            elseif num > 1000 then
                return string.format("%.1f", num / 1000) .. " M"
            else
                return string.format("%.1f", num) .. " K"
        	end
    	end
    	local eth_iface = config.is_desktop and "enp3s0" or "enp57s0f1"
    	local wifi_iface = config.is_desktop and "wlp4s0" or "wlp0s20f3"
    	local state = ""
    	if net_now.devices and net_now.devices[wifi_iface] and net_now.devices[wifi_iface].state ~= "down" then
            state = "Ξ"
        elseif net_now.devices and net_now.devices[eth_iface] and net_now.devices[eth_iface].state ~= "down" then
            state = markup.fg.color("green", "⍽")
        else
        	state = markup.fg.color("red", "ø")
    	end
        local status = state .. " ↑ " .. unit(net_now.sent) .. ", ↓ " .. unit(net_now.received)
        widget:set_markup(markup.font(theme.font, status))
    end
}

-- Temp
local temp = lain.widget.temp {
    tempfile = config.is_desktop and "/sys/devices/virtual/thermal/thermal_zone2/temp" or nil,
    settings = function()
        local status = "temp: " .. coretemp_now .. "°C"
        widget:set_markup(markup.font(theme.font, status))
    end
}

-- GPU Temp
local gpu_temp = lain.widget.temp {
    tempfile = "/sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0/hwmon/hwmon5/temp1_input",
    settings = function()
    	local status = "GPU: " .. coretemp_now .. "°C"
    	widget:set_markup(markup.font(theme.font, status))
    end
}

-- CPU
local cpu = lain.widget.cpu {
    settings = function()
        local status = "cpu: " .. cpu_now.usage .. "%"
        widget:set_markup(markup.font(theme.font, status))
    end
}

-- MEM
local mem = lain.widget.mem {
    settings = function()
        local status = "mem: " .. mem_now.perc .. "%"
        widget:set_markup(markup.font(theme.font, status))
    end
}

-- Battery
local bat = lain.widget.bat({
  settings = function()
    bat_header = bat_now.perc .. "% "
    if bat_now.ac_status == 1 then
      bat_header = "🗲 " .. bat_header
    else
      bat_header = bat_header .. bat_now.time .. " "
    end
    widget:set_markup(markup.font(theme.font, bat_header))
  end
})

-- / fs
-- commented because it needs Gio/Glib >= 2.54
theme.fs = lain.widget.fs()
--

-- ALSA volume bar
theme.volume = lain.widget.alsa({
  togglechannel = "Master,0",
  settings = function()
    local status = "vol: "
    if volume_now.status == "off" then
      status = status .. "m"
    else
      status = status .. volume_now.level .. "%"
    end
    widget:set_markup(markup.font(theme.font, status))
  end
})

-- Weather
--theme.weather = lain.widget.weather({
--  city_id = 6077243, -- placeholder (London)
--  notification_preset = { position = "bottom_right" },
--})

-- Separators
local separators = lain.util.separators
local arrow = separators.arrow_left

local function pla(widget)
  return wibox.container.margin(widget, dpi(16), dpi(16))
end

function theme.at_screen_connect(s)
  -- Quake application
  -- s.quake = lain.util.quake({ app = awful.util.terminal })

  -- If wallpaper is a function, call it with the screen
  local wallpaper = theme.wallpaper
  if type(wallpaper) == "function" then
    wallpaper = wallpaper(s)
  end
  gears.wallpaper.maximized(wallpaper, s, true)

  -- Tags
  awful.tag(awful.util.tagnames, s, awful.layout.layouts)

  -- Create a taglist widget
  s.mytag = awful.widget.taglist {
    screen = s,
    filter = awful.widget.taglist.filter.all,
    buttons = awful.util.taglist_buttons,
    layout = wibox.layout.fixed.horizontal,
    -- Notice that there is *NO* wibox.wibox prefix, it is a template,
    -- not a widget instance.
    widget_template = {
      {
        {
          {
            id     = 'text_role',
            widget = wibox.widget.textbox,
          },
          left = dpi(15),
          right = dpi(15),
          widget = wibox.container.margin,
          id = 'text_margin_role'
        },
        bg = theme.bg_normal,
        id = 'background_role',
        widget = wibox.container.background,
      },
      bottom = dpi(1),
      widget = wibox.container.margin,
    },
  }

  -- Create the wibox
  s.mywibox = awful.wibar({ bg = "#0000", position = "top", screen = s, height = dpi(32) })

  -- Add widgets to the wibox
  s.mywibox:setup {
    {
      layout = wibox.layout.align.horizontal,
      expand = 'none',
      { -- Left widgets
        layout = wibox.layout.fixed.horizontal,
        pla(s.mytag),
        pla(wibox.widget.systray()),
        pla(config.is_desktop and gpu_temp.widget or bat.widget),
      },
      { -- Middle widget
        layout = wibox.layout.fixed.horizontal,
        textcal,
      },
      { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        --theme.mail.widget,
        pla(net.widget),
        pla(temp.widget),
        pla(cpu.widget),
        pla(mem.widget),
        pla(theme.volume.widget),
      },
    },
    widget = wibox.container.margin,
    top = dpi(8),
  }
end

return theme
