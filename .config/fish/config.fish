starship init fish | source
direnv hook fish | source

alias l 'exa -al --group-directories-first --git'
abbr df 'df -hH --total'
abbr y 'yarn'
alias f 'rg --trim -S --sort path'
abbr e 'kak'
abbr c 'git add . && git commit -v && git push --set-upstream origin HEAD'
alias xclip 'xclip -selection clipboard'

alias nbe 'sudo kak /etc/nixos/configuration.nix && sudo nixos-rebuild switch'
abbr nbs 'sudo nixos-rebuild switch'
abbr nup 'sudo nix-channel --update'
abbr ngc 'sudo nix-collect-garbage --delete-older-than 10d'
alias feh 'feh --conversion-timeout 3 -d -Z --image-bg \'#333333\' --scale-down'
alias top 'zenith --disable-history --cpu-height 0 --disk-height 0 --net-height 0 --refresh-rate 1000'

function p
  set _P (find ~/.password-store/ -type f -printf '%P\n' | sed 's/\.[^.]*$//' | fzf)
  if [ -n "$_P" ];
      pass -c $_P
      pass $_P | tail +2
  end
end
alias pf 'pass -c firefox'

alias ssh='env TERM=xterm-256color \ssh'

alias sshlux="ssh lux@(arp -a | grep 90:e8:68:83:f1:f1 | cut -f2 -d' ' | tr -d '()')"

export EDITOR=kak
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="/opt/tools/bsc/latest/bin:$PATH"
export PAGER=bat
export DELTA_PAGER="less -RFix4 --mouse"
export BAT_THEME='Solarized (light)'

fish_add_path /home/adminxvii/.spicetify
