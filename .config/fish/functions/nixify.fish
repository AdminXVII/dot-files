function nixify --description 'Shell function to quickly setup nix + direnv in a new project'
  if test ! -e ./.envrc
    echo "use nix" > .envrc
    direnv allow
  end
  if test ! -e shell.nix
    echo "\
with import <nixpkgs> {};
stdenv.mkDerivation {
  name = \"env\";
  buildInputs = [
    hello
  ];
}
" > shell.nix
    $EDITOR shell.nix
  end
end
